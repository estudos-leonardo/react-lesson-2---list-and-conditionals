import React from 'react';

const charComponent = (props) => {

    const style = {
        display: 'inline-block',
        padding: '16px',
        textAlign: 'center',
        margin: '16px',
        border: '1px solid black',
        cursor: 'pointer'
    }

    return(
        <div className="char-component" style={style} onClick={props.clicked}>
            {props.char}
        </div>
    )
};

export default charComponent;