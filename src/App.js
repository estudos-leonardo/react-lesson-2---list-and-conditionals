import React, { Component } from 'react';
import ValidationComponent from './ValidationComponent/ValidationComponent';
import CharComponent from './CharComponent/CharComponent';
import './App.css';

class App extends Component {
	
	state = {
		text: '',
		textLen: 0,
		charList: []
	}

	processText = (e) => {
		e.preventDefault();

		let text = e.target.value;
		let textLen = text.length;
		let charList = text.split('');

		this.setState({
			text: text,
			textLen: textLen,
			charList: charList
		})
	}

	removeChar = (index) => {
		
		const charList = [...this.state.charList];
		charList.splice(index, 1);
		
		let newText = charList.join('');

		this.setState({
			text: newText,
			charList: charList
		})
	}
	
	render() {
		return (
			<div className="App">
			<input type="text" onChange={this.processText} value={this.state.text} />
			<ValidationComponent textLength={this.state.textLen}/>
			{
				this.state.charList.map((char, index) =>{
					return (
						<CharComponent key={index} char={char} clicked={() => this.removeChar(index)}/>
					)
				})
			}
			</div>
			);
		}
	}
	
	export default App;
