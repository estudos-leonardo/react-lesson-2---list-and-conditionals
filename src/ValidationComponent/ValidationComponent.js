import React from 'react';

const validationComponent = (props) => {

    const validStyle = {color: 'green'}
    const invalidStyle = {color: 'red'}
    const minChars = 5;
    let style = {};
    let validatorText = '';
    
    if (props.textLength < 1){
        validatorText = 'Input some text!';
    } else if(props.textLength < minChars) {
        style = invalidStyle;
        validatorText = 'Text too short!';
    } else {
        style = validStyle;
        validatorText = 'Text long enough!';
    }

    return (
        <p style={style}>{validatorText}</p>
    )
};

export default validationComponent;